import numpy as np
import os.path
import time
import sys
import time
import pyprind
from align import AlignDlib
import dlib
import cv2
from imutils import face_utils

import pickle

from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import train_test_split

import bz2
import os
from PIL import Image

from urllib.request import urlopen

def download_landmarks(dst_file):
    url = 'http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2'
    decompressor = bz2.BZ2Decompressor()
    
    with urlopen(url) as src, open(dst_file, 'wb') as dst:
        data = src.read(1024)
        while len(data) > 0:
            dst.write(decompressor.decompress(data))
            data = src.read(1024)

dst_dir = 'data/models'
dst_file = os.path.join(dst_dir, 'landmarks.dat')

if not os.path.exists(dst_file):
    os.makedirs(dst_dir)
    download_landmarks(dst_file)

class IdentityMetadata():
    def __init__(self, base, name, file):
        # dataset base directory
        self.base = base
        # identity name
        self.name = name
        # image file name
        self.file = file

    def __repr__(self):
        return self.image_path()

    def image_path(self):
        return os.path.join(self.base, self.name, self.file) 
    
def load_metadata(path):
    metadata = []
    for i in os.listdir(path):
        list_files = os.listdir(os.path.join(path, i))
        if len(list_files) > 10:
            for f in list_files:
                # Check file extension. Allow only jpg/jpeg' files.
                ext = os.path.splitext(f)[1]
                if ext == '.jpg' or ext == '.jpeg':
                    metadata.append(IdentityMetadata(path, i, f))
    return np.array(metadata)

def load_image(path):
    img = cv2.imread(path, 1)
    return img[...,::-1]

def align_image(img):
    alignment = AlignDlib('data/models/landmarks.dat')
    return alignment.align(96, img, alignment.getLargestFaceBoundingBox(img), 
                           landmarkIndices=AlignDlib.OUTER_EYES_AND_NOSE)

def detect_face(metadata,output = 'data/image_face/'):
    detector = dlib.get_frontal_face_detector()
    bar = pyprind.ProgBar(len(metadata),bar_char='█')
    for i, m in enumerate(metadata):
        img = cv2.imread(m.image_path())
        img = align_image(img)
        faces = detector(img)
        if len(faces) > 1:
            try:
                face = faces[0]
                (x, y, w, h) = face_utils.rect_to_bb(face)
                face_img = img[y:y + h, x:x + w]
                path = output+m.name+'/'
                if not os.path.exists(path):
                    os.mkdir(path)
                cv2.imwrite(path+m.file, face_img)
            except:
                pass
        bar.update()

def _read_image_as_array(path, dtype):
    f = Image.open(path)
    try:
        image = np.asarray(f, dtype=dtype)
    finally:
        # Only pillow >= 3.0 has 'close' method
        if hasattr(f, 'close'):
            f.close()
    return image


def _postprocess_image(image):
    if image.ndim == 2:
        # image is greyscale
        image = image[..., None]
    return image



def load_dataset(path,test_size=0.2):
    print('Load metadata...')
    metadata = load_metadata(path)
    
    
    img = np.empty((len(metadata), 224, 224, 3))
    labels = []
    label_encoded = np.empty((len(metadata),11), dtype=np.uint8)
    
    bar = pyprind.ProgBar(len(metadata),bar_char='█')
    print('Read image...')
    i = 0
    for img_meta in metadata:
        image = _read_image_as_array(img_meta.image_path(), None)
        image = cv2.resize(image,(224,224))
        image = _postprocess_image(image)
        image = (image / 255.)
        label = img_meta.name
        img[i] = image
        labels.append(label)
        i += 1
        bar.update()
    label_encoder = LabelEncoder()
    integer_encoded = label_encoder.fit_transform(labels)
    onehot_encoder = OneHotEncoder(categories='auto',sparse=False)
    integer_encoded = integer_encoded.reshape(len(integer_encoded), 1)
    onehot_encoded = onehot_encoder.fit_transform(integer_encoded)

    x = img
    y = onehot_encoded

    xTrain, xTest, yTrain, yTest = train_test_split(x, y, test_size = test_size, random_state = None)

    print(f'Total images loaded: {len(metadata)}\nTrain: {len(xTrain)}\nTest: {len(xTest)}')
    

    return xTrain, xTest, yTrain, yTest, label_encoder

if  __name__ == '__main__':
     xTrain, xTest, yTrain, yTest, label_encoder  = load_dataset('data/images',0.1)
     
     print(xTrain[0].shape)
     print(yTrain[0].shape)
     print(yTrain[0])
       
     inverted = label_encoder.inverse_transform([np.argmax(yTrain[0])])[0]
     print(inverted)
     
