import cv2
import numpy as np
import os, time
import dlib
from imutils import face_utils
from imutils.face_utils import FaceAligner
import sys
from imutils import build_montages

import time
import pickle

from keras.models import load_model

model = load_model('data/pretrained/_vgg16_.15-0.97.hdf5', compile=False)
label_encoder = pickle.load(open('data/models/labelencoder.sav','rb'))

def recognize(img):
    start_time = time.time()
    x = (img/225.)
    prediction = model.predict(np.expand_dims(x, axis=0))[0]
    respond_time = time.time() - start_time
    label = label_encoder.inverse_transform([np.argmax(prediction)])[0]
    
    return label,prediction[np.argmax(prediction)], respond_time

class VideoCamera(object):
    def __init__(self):
        self.video = cv2.VideoCapture(0)

        self.detector = dlib.get_frontal_face_detector()
        # self.shape_predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")
    
    def __del__(self):
        self.video.release()
    
    def get_frame(self):
        success, image = self.video.read()

        image = horizontal_img = cv2.flip( image,1)
        faces = self.detector(image)

        if (len(faces) > 0):
            for face in faces:
                    try:
                        (fX, fY, fW, fH) = face_utils.rect_to_bb(face)
                        roi = image[fY:fY + fH, fX:fX + fW]
                        roi = cv2.resize(roi, (224, 224))
                        
                        #identity, conf, respond_time = recognize(roi)
                        identity = 'Si x'

                        cv2.putText(image, identity , (fX, fY - 10),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)
                        

                        cv2.rectangle(image, (fX, fY), (fX + fW, fY + fH),
                        (244,212,66), 2)
                    except:
                        pass
        

        ret, jpeg = cv2.imencode('.jpg', image)
        return jpeg.tobytes()
