from flask import Flask, render_template, Response
from camera import VideoCamera

app = Flask(__name__)

def gen(camera):
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')


'''
Route
'''

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/realtime_camera')
def realtime_camera():
    return render_template('realtime.html')

@app.route('/login')
def login():
    return render_template('login.html')

@app.route('/video_feed')
def video_feed():
    return Response(gen(VideoCamera()),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

if __name__ == '__main__':
    # app.run(host='127.0.0.1', debug=True)
    app.run()
