from deepnet import optimizers
from deepnet.callbacks import CSVLogger, ModelCheckpoint, EarlyStopping
from deepnet.callbacks import ReduceLROnPlateau

from finetuned_model import vgg16_finetuned
from data import load_dataset

import pickle

import argparse

def main(data, num_classes, batch_size, epochs):
    xTrain, xTest, yTrain, yTest, label_encoder = load_dataset(data)

    model = vgg16_finetuned(num_classes)
    model.compile(optimizer='adam', loss='categorical_crossentropy',
              metrics=['accuracy'])
    

    # callbacks
    base_path = 'data/'
    patience = 50
    log_file_path = base_path + 'logs/training.log'
    csv_logger = CSVLogger(log_file_path, append=False)
    reduce_lr = ReduceLROnPlateau('val_loss', factor=0.1,
                                    patience=int(patience/4), verbose=1)
    early_stop = EarlyStopping('val_loss', patience=patience)
    trained_models_path = base_path + 'pretrained/_vgg16_'
    model_names = trained_models_path + '.{epoch:02d}-{val_acc:.2f}.hdf5'
    model_checkpoint = ModelCheckpoint(model_names, 'val_loss', verbose=1,
                                                        save_best_only=True)
    callbacks = [model_checkpoint, csv_logger,reduce_lr, early_stop]

    filename = 'data/models/labelencoder.sav'
    pickle.dump(label_encoder, open(filename, 'wb'))

    model.fit(xTrain, yTrain,
                epochs=epochs,
                batch_size=batch_size,
                validation_data=(xTest, yTest),
                callbacks=callbacks)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Train Face Face Model')
    parser.add_argument('--datasets', '-d', default='data/images/',
                        help='Dataset directory')
    parser.add_argument('--numclasses', '-n', type=int, default=11,
                        help='Number of classes')
    parser.add_argument('--batchsize', '-b', type=int, default=16,
                        help='Number of images in each mini-batch')
    parser.add_argument('--epochs', '-e', type=int, default=100,
                        help='Number of sweeps over the dataset to train')
    args = parser.parse_args()

    main(args.datasets,args.numclasses,args.batchsize,args.epochs)

